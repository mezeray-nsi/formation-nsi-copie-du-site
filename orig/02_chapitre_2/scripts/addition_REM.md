!!! info "Paramètres"

    Lorsqu'on écrit `addition(a, b)` a et b s'appellent les **paramètres** de la fonction `addition`
