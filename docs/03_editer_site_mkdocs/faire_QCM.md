# Des QCMs

## Le principe
Le thème propose une macro `multi_qcm`, qui permet de construire des groupes de questions à choix simples ou multiples, avec évaluation automatique du résultat, avec ou sans affichage de la correction.

Pour tester ses réponses, il suffit de cocher ses choix, puis de cliquer sur le bouton de vérification : le compteur de réponses correctes s'affiche alors.

Les qcms sont "rejouables" à volonté (bouton `recommencer`) et ont donc valeur d'évaluation formative.

{{ multi_qcm(
            [
            "Quelle est la réponse ?",
            ["4", "2", "42", "0"],
            [3]
            ],
            qcm_title = "Un Qcm avec une seule question",
            multi = False,
            DEBUG = False
        ) 
}}  

??? info "La documentation complète"
    [**tout sur les qcm**](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcms/){target="_blank"}

## La syntaxe
La macro `{% raw %} {{ multi_qcm({% endraw %}...)}}` prend en argument, pour chaque question, une liste comprenant trois ou quatre éléments :   

* la **question**, de type str qui peut être du markdown avec du code, du latex (mais attention, il faudra doubler les `\`) ; par exemple : `#!py "Quelle est la réponse ?"`
* une **liste de réponses**  ; par exemple : `#!py ["4", "2", "42"]` 
* une liste contenant **le numéro de la bonne réponse ou des bonnes réponses**  ; par exemple : `#!py [3]`. Attention, ce n'est pas l'indice !
* un dictionnaire (optionnel) qui contient des options spécifiques à cette question. À l'heure actuelle, une seule option est couverte `'multi': bool` (voir ci-dessous)

Il faut également renseigner obligatoirement les deux arguments :

* `multi` : réglage pour **toutes** les questions du qcm, indiquant si les questions n'ayant qu'une seule bonne réponse sont à choix multiples ou pas. 
* `DEBUG` : affiche ou non dans la console le code markdown généré pour ce qcm durant le build.       

Il est conseillé de renseigner aussi l'argument `qcm_title` qui par défaut vaut `#!py 'Question'`. Il y a d'autres arguments facultatifs que l'on verra plus loin.
    
    
!!! example "Le code de l'exemple complet"
    === "le code"    
        ````markdown
        {% raw %}    
        {{ multi_qcm(
            [
            "Quelle est la réponse ?",
            ["4", "2", "42"],
            [3]
            ],
            qcm_title = "Un Qcm avec une seule question",
            multi = False,
            DEBUG = False
        ) }}
        {% endraw %}
        ````    
    === "le resultat"
    
        {{ multi_qcm(
            [
            "Quelle est la réponse ?",
            ["4", "2", "42", "0"],
            [3]
            ],
            qcm_title = "Un Qcm avec une seule question",
            multi = False,
            DEBUG = False
        ) }}   
        
!!! note "Remarque"
    On remarquera que les boutons réponses sont des boutons radio. Pour `#!py multi = True`, les boutons réponses sont des cases à cocher.       

## Les paramètres optionnels

On peut renseigner, en plus, les options suivantes :

|paramètre|type|valeur par défaut|description|
| :--- | :---: | :---: |:--- |
| `hide` |`#!py bool`|`#!py False`|si on la met à `#!py True`, un masque apparaît au-dessus des boutons pour signaler à l'utilisateur que les réponses resteront cachées après validation.|
|`shuffle` |`#!py bool`|`#!py False`|si on la met à `#!py True`, les questions et les réponses seront mélangées.|
|`admo_kind` |`#!py str`|`#!py '!!!'`|type d'admonition utilisée pour les questions ('???' et '???+' sont également utilisables).|
|`admo_class` |`#!py str`|`'tip'`|classe d'admonition|

## Exemples


!!! example "QCM avec une seule bonne réponse et avec correction"
    === "le code"
        ````markdown
        {% raw %}
        {{ multi_qcm(
            [
            """
            On a saisi le code suivant :
            ```python title=''
            n = 8
            while n > 1:
                n = n/2
            ```
            
            Que vaut `n` après l'exécution du code ?
            """,
            [
            "2.0",
            "4.0",
            "1.0",
            "0.5",
            ],
            [3]
            ],
            multi = False,
            qcm_title = "Un QCM avec mélange automatique des réponses",
            DEBUG = False,
            shuffle = True,
            admo_class = 'question'
        ) }}
        {% endraw %}
        ````
            
    
    
    === "le résultat"
        {{ multi_qcm(
            [
            """
            On a saisi le code suivant :
            ```python title=''
            n = 8
            while n > 1:
                n = n/2
            ```
            
            Que vaut `n` après l'exécution du code ?
            """,
            [
            "2.0",
            "4.0",
            "1.0",
            "0.5",
            ],
            [3]
            ],
            multi = False,
            qcm_title = "Un QCM avec mélange automatique des réponses",
            DEBUG = False,
            shuffle = True,
            admo_class = 'question'
        ) }}


!!! example "QCM avec une seule bonne réponse et sans correction"
    Seul le score indiquera combien il y a de bonnes réponses. 
    
    === "le code"
        ````markdown
        {% raw %}
        {{ multi_qcm(
            [
            """
            On a saisi le code suivant :
            ```python title=''
            n = 8
            while n > 1:
                n = n/2
            ```
            
            Que vaut `n` après l'exécution du code ?
            """,
            [
            "2.0",
            "4.0",
            "1.0",
            "0.5",
            ],
            [3]
            ],
            [
            "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
            [
            "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
            "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
            "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
            "Le serveur web sur lequel est stockée la page HTML."
            ],
            [1],
            ],
            multi = False,
            qcm_title = "Un QCM avec mélange automatique des questions et réponses (bouton en bas pour recommencer)",
            DEBUG = False,
            shuffle = True,
            admo_class = 'question',
            hide = True
        ) }}
        {% endraw %}
        ````
            
    
    
    === "le résultat"
        {{ multi_qcm(
            [
            """
            On a saisi le code suivant :
            ```python title=''
            n = 8
            while n > 1:
                n = n/2
            ```
            
            Que vaut `n` après l'exécution du code ?
            """,
            [
            "2.0",
            "4.0",
            "1.0",
            "0.5",
            ],
            [3]
            ],
            [
            "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
            [
            "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
            "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
            "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
            "Le serveur web sur lequel est stockée la page HTML."
            ],
            [1],
            ],
            multi = False,
            qcm_title = "Un QCM avec mélange automatique des questions et réponses (bouton en bas pour recommencer)",
            DEBUG = False,
            shuffle = True,
            admo_class = 'question',
            hide = True            
        ) }}

 



!!! example  "QCM avec une ou plusieurs réponses selon les questions"
    Si on souhaite mélanger des réponses unique ou multiples, il faut choisir la valeur de `multi` par défaut et indiquer dans un dictionnaire le choix particulier par question.
    Ici on a choisi `#!py multi = False` pour le comportement global et on ajoute `#!py {'multi': True}` dans la liste de la question concernée.

    === "le code"
        ````markdown
        {% raw %}
        {{ multi_qcm(
        [
        """
        On a saisi le code suivant :
        ```python title=''
        n = 8
        while n > 1:
            n = n/2
        ```

        Que vaut `n` après l'exécution du code ?
        """,
        [
        "2.0",
        "4.0",
        "1.0",
        "0.5",
        ],
        [3]
        ],
        [
        "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
        [
        "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
        "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
        "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
        "Le serveur web sur lequel est stockée la page HTML."
        ],
        [1],
        ],
        [
        """
        Cocher toutes les bonnes réponses
        ```python title=''
        meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']
        ```
        """,
        [
        "`#!py meubles[1]` vaut `#!py Table`",
        "`#!py meubles[1]` vaut `#!py Commode`",
        "`#!py meubles[4]` vaut `#!py Buffet`",
        "`#!py meubles[5]` vaut `#!py Buffet`",
        ],
        [2, 3],
        {'multi': True}
        ],
        multi = False,
        qcm_title = "Un QCM avec mélange automatique des questions (bouton en bas pour recommencer)",
        DEBUG = False,
        shuffle = True
        ) }} 
        {% endraw %}
        ````
          
    === "le résultat"
        {{ multi_qcm(
        [
        """
        On a saisi le code suivant :
        ```python title=''
        n = 8
        while n > 1:
            n = n/2
        ```

        Que vaut `n` après l'exécution du code ?
        """,
        [
        "2.0",
        "4.0",
        "1.0",
        "0.5",
        ],
        [3]
        ],
        [
        "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
        [
        "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
        "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
        "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
        "Le serveur web sur lequel est stockée la page HTML."
        ],
        [1],
        ],
        [
        """
        Cocher toutes les bonnes réponses
        ```python title=''
        meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']
        ```
        """,
        [
        "`#!py meubles[1]` vaut `#!py Table`",
        "`#!py meubles[1]` vaut `#!py Commode`",
        "`#!py meubles[4]` vaut `#!py Buffet`",
        "`#!py meubles[5]` vaut `#!py Buffet`",
        ],
        [2, 3],
        {'multi': True}
        ],
        multi = False,
        qcm_title = "Un QCM avec mélange automatique des questions (bouton en bas pour recommencer)",
        DEBUG = False,
        shuffle = True
        ) }}

