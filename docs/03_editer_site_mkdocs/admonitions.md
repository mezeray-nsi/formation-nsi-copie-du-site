# Les admonitions

## Le principe
Les [admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/#admonitions){target='_blank'} permettent d'isoler un contenu tout en lui attribuant une fonction : avertissement, info, exemple, question, indice, etc.
	
Les admonitions ont un titre par défaut en anglais. Pour personnaliser un titre, il suffit de le rajouter entre guillemets. Un titre vide permet d'effacer la ligne de titre.    
Contrairement, aux admonitions présentes dans les notebooks sur basthon/Capytale, le contenu des admonitions est indenté dans Mkdocs : il n'y a pas de marque indiquant la fin si ce n'est l'absence d'indentation. 
De plus, les admonitions peuvent contenir tout type de contenu, y compris d'autres admonitions.
	

!!! quote ""
	=== "le code"
		```markdown  
		!!! info "Mon info"
			
			Le contenu doit être indenté 
		```
	=== "le résultat"
		!!! info "Mon info"
			
			Le contenu doit être indenté 
 

## Types disponibles

??? note "Cliquer pour voir les différentes admonitions "
	!!! info
		```markdown  
		!!! info  
		```


	!!! warning  
		```markdown  
		!!! warning   
		```
	 
	!!! danger 
		```markdown  
		!!! danger
		```

	!!! note 
		```markdown  
		!!! note
		```

	!!! abstract
		```markdown  
		!!! abstract
		```

	!!! tip 
		```markdown  
		!!! tip
		```

	!!! question
		```markdown  
		!!! question
		```

	!!! example
		```markdown  
		!!! example
		```		
	!!! success
		```markdown  
		!!! success
		```

	!!! failure
		```markdown  
		!!! failure
		```
		
	!!! bug
		```markdown  
		!!! bug
		```	
		
	!!! quote 
		```markdown  
		!!! quote
		```
 
 
## Admonitions pliées/dépliées

!!! example "Admonition à déplier"
	=== "le code"
		```markdown 
		??? note "se déroule en cliquant dessus"

			Ma note indentée
		```
	=== "le résultat" 
		??? note "se déroule en cliquant dessus"

			Ma note indentée
 


!!! example "Admonition repliable dépliée"
	=== "le code"
	    ```markdown 
		???+ note "cliquer pour replier"

			Mon texte indenté
		```
 	=== "le résultat"  
		???+ note "cliquer pour replier"

			Mon texte indenté
 
## Admonitions inline, à gauche ou à droite
!!! example "Admonition dans la marge gauche"
 	=== "le code"
		```markdown 
		!!! question inline "Question à gauche"

			Le contenu de l'admonition doit être indenté 
			
		Le texte non indenté se place à droite de la question.  
		Il est en dehors de l'admonition.  
		Il doit être assez long    
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.   
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.	  
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.	
		```
 	=== "le résultat"  
		!!! question inline "Question à gauche"

			Le contenu de l'admonition doit être indenté 
			
		Le texte non indenté se place à droite de la question.  
		Il est en dehors de l'admonition.  
		Il doit être assez long    
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.   
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.   
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.


!!! example "Admonition dans la marge droite"
 	=== "le code" 
		```markdown  
		??? note inline end "Note à droite"

			Texte de la note indenté
			
		Le texte non indenté se place à gauche de la question.  
		Il est en dehors de l'admonition.  
		Il doit être assez long    
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.   
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.	  
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.	
		```
 	=== "le résultat"
		??? note inline end "Note à droite"

			Texte de la note indenté
			
		Le texte non indenté se place à gauche de la question.  
		Il est en dehors de l'admonition.  
		Il doit être assez long    
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa. 
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.	  
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
		nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
		massa, nec semper lorem quam in massa.	

 
## Onglets ou panneaux coulissants

!!! info "Panneaux coulissants"
 	=== "le code" 
		```markdown  
		Exemples avec trois panneaux

		=== "Panneau 1"
			Ici du texte concernant ce panneau 1

			Il peut prendre plusieurs lignes

		=== "Panneau 2"
			Ici du texte concernant ce panneau 2

			Il peut prendre plusieurs lignes

		=== "Panneau 3"
			Ici du texte concernant ce panneau 3

			Il peut prendre plusieurs lignes 
		```  
 	=== "le résultat"   
		Exemples avec trois panneaux
		!!! quote ""

			=== "Panneau 1"
				Ici du texte concernant ce panneau 1

				Il peut prendre plusieurs lignes

			=== "Panneau 2"
				Ici du texte concernant ce panneau 2

				Il peut prendre plusieurs lignes

			=== "Panneau 3"
				Ici du texte concernant ce panneau 3

				Il peut prendre plusieurs lignes



 
