On peut aussi parcourir le tableau `nombres` avec l'indice, mais c'est moins simple !
```python
def somme(nombres):
	resultat = 0
	for indice in range(len(nombres)):
		resultat = resultat + nombres[indice]
	return resultat
```
