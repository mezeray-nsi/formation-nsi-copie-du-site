
# --------- PYODIDE:code --------- #

def somme(nombres):
    """
    nombres est un tableau (type list) non vide contenant des entiers (type int) 
    la fonction renvoie la somme des éléments du tableau 
    """
    pass


# --------- PYODIDE:corr --------- #

def somme(nombres):
    """
    nombres est un tableau (type list) non vide contenant des entiers (type int) 
    la fonction renvoie la somme des éléments du tableau 
    """
    resultat = 0
    for nb in nombres:
        resultat = resultat + nb
    return resultat


# --------- PYODIDE:tests --------- #

assert somme([]) == 0
assert somme([1, 2, 3]) == 6
assert somme([5, 6, 7]) == 18


# --------- PYODIDE:secrets --------- #

assert somme([5, 6, 7, 0, 100]) == 118
