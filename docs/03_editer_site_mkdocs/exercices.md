---
title: Des exercices avec IDE
---
    

Les IDE proposés par `pyodide-mkdocs-theme` sont un environnement clé en main avec :

* Un éditeur de code, très similaire à ce qu'on peut trouver dans des éditeurs de type VSC ;
* Un terminal, qui fonctionne de la même manière qu'une console interactive python ;
* Un jeu de boutons et fonctionnalités permettant de faire tourner l'ensemble, avec pyodide travaillant en sous-main.

!!! info  "Rappel RGPD"

    L'intégralité de l'environnement tourne dans le navigateur, côté client. Il n'y a aucune donnée renvoyée au serveur.


??? abstract "La documentation complète"
    [Documentation détaillée de pyodide-mkdocs-theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){ target="_blank" } par Frédéric Zinelli.

 
## Un terminal
 
### Un terminal python dans la page
On peut créer un terminal python vide.   
!!! quote ""
    === "le code"
        ```markdown
        {% raw %}
        {{ terminal() }}
        {% endraw %}
        ```
    === "Le résultat"

        {{ terminal() }}

L'auto-complétion avec ++tab++, le rappel de ce qui précède avec ++up++ et le rappel de l'historique (avec ++ctrl+"R"++ ) sont possibles.    
++f5++ rafraîchit la page mais conserve l'historique.

### Un deuxième terminal
    
Les deux terminaux de la page ne sont pas indépendants. Si vous avez défini une variable dans le premier, elle peut être utilisée dans le deuxième.

{{ terminal() }}

### Un terminal avec un fichier caché
    
On peut lier un fichier `.py` contenant des fonctions qui pourront être utilisées dans le terminal.    
On indiquera l'adresse relative entre guillemets simples et sans l'extension. 

L'option `FILL` permet de fournir un contenu à afficher dans le terminal.

!!! quote ""
    === "le code dans le fichier md"
        ```markdown
        {% raw %}
        {{ terminal('scripts/exo_term', FILL="somme_10()" ) }}
        {% endraw %}
        ```
    === "le résultat"

        {{ terminal('scripts/exo_term', FILL="somme_10()" ) }}

    === "contenu du fichier python"
        ```python
        # --------- PYODIDE:env_term --------- #

        def somme_10():
            somme = 0
            for nb in range(1, 11):
                somme += nb
            return somme
        ```


!!! warning "Attention à la syntaxe"
    La ligne : `# --------- PYODIDE:env_term --------- #` n'est pas un commentaire, et n'est pas optionnelle. Elle définit une section `env_term` pour le fonctionnement de Pyodide. On peut éventuellement écrire  `# - PYODIDE:env_term - #`,  mais pas `# PYODIDE:env_term #`

??? abstract "Lien vers la documentation"
    [**Tout sur les terminaux**](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/terminaux/){ target="_blank"  }
    

## Un IDE

### Un IDE vide
On peut créer un IDE vide, visuellement proche de Thonny. La zone de saisie se redimensionne automatiquement et autorise l'auto-complétion avec ++ctrl+space++.
Des bulles informatives indiquent la fonction des boutons de l'interface, au survol de la souris.

!!! quote ""
    === "le code"
        ```markdown
        {% raw %}
        {{ IDE() }}
        {% endraw %}
        ```
    === "le résultat"
        {{ IDE() }}


    On peut avoir aussi un IDE vertical.
    === "le code" 
        ```markdown
        {% raw %}
        {{ IDEv() }}
        {% endraw %}
        ```  
    === "le résultat"
        {{ IDEv() }}


### Un IDE affichant du code

On peut afficher le code d'un fichier `.py` dans l'IDE. Dans l'exemple ci-dessous, le fichier `somme1.py` se trouvant dans le dossier `scripts` est chargé dans l'IDE. Le contenu de la section `code` de ce fichier est affiché dans l'IDE. 

!!! quote ""
    === "le code dans le fichier md"
        ```markdown
        Compléter la fonction :    
        {% raw %}
        {{ IDE('scripts/somme1') }}
        {% endraw %}
        ```
    === "le résultat"
        Compléter la fonction :
        
        {{ IDE('scripts/somme1') }}
                
    === "contenu du fichier python"
        ```python
        # --------- PYODIDE:code --------- #

        def somme(nombres):
            """
            nombres est un tableau (type list) non vide contenant des entiers (type int) 
            la fonction renvoie la somme des éléments du tableau 
            """
            pass
        ```
 
 Le fichier `somme1.py` ne contenant pas de section de tests, ni de correction, le compteur d'essais est automatiquement passé à $\infty /\infty$ 
        

### Un IDE avec code, tests et correction

On peut également proposer des tests de validation de la solution proposée par un utilisateur. Un seul fichier `.py` contient à la fois le code, le corrigé et les tests.  Dans l'exemple ci-dessous, le fichier `somme2.py` se trouvant dans le dossier `scripts` est chargé dans l'IDE :

- La section `code` contient le code affiché dans l'IDE ;
- La section `corr` contient le code d'un corrigé possible ;
- La section  `tests` contient des tests publics : le contenu de cette section est également affiché dans l'IDE sous le code : l'exécution du code permettra de passer ces tests ;
- enfin, la section `secrets` contient des tests cachés : c'est le bouton de validation qui déclenche leur exécution. L'accès au corrigé est déclenché si ces tests sont passés avec succès, ou si le décompte des validations restantes atteint 0.

!!! quote ""
    === "le code dans le fichier md"
        ```markdown
        
        Compléter le script ci-dessous.
        N'oubliez pas de valider les tests après avoir exécuté.    
        {% raw %}
        {{ IDE('scripts/somme2') }}
        {% endraw %}
        ```
    === "le résultat"
        
        {{ IDE('scripts/somme2') }}

    === "contenu du fichier python"
        Voici le contenu du fichier `somme2.py`" 
        ```python
        # --------- PYODIDE:code --------- #

        def somme(nombres):
            """
            nombres est un tableau (type list) non vide contenant des entiers (type int) 
            la fonction renvoie la somme des éléments du tableau 
            """
            pass


        # --------- PYODIDE:corr --------- #

        def somme(nombres):
            """
            nombres est un tableau (type list) non vide contenant des entiers (type int) 
            la fonction renvoie la somme des éléments du tableau 
            """
            resultat = 0
            for nb in nombres:
                resultat = resultat + nb
            return resultat


        # --------- PYODIDE:tests --------- #

        assert somme([]) == 0
        assert somme([1, 2, 3]) == 6
        assert somme([5, 6, 7]) == 18


        # --------- PYODIDE:secrets --------- #

        assert somme([5, 6, 7, 0, 100]) == 118
        ```
    
!!! warning "Attention à la section `secrets`"

    S'il y a une section `corr` ou un fichier de remarques (voir plus loin), le fichier python doit **absolument** comporter la section `secrets`.

    En l'absence de la section `secrets` le pipeline sera mis en échec.

### Paramètres optionnels

#### Nombre d'essais

Par défaut, les utlisateurs peuvent valider leur exercice 5 fois avant que la correction ne s'affiche.     
On peut gérer le nombre d'essais en utilisant le paramètre `MAX` lors de l'appel de la macro `IDE`.

#### Interdire 

On peut interdire l'utilisation des fonctions builtins (`#!py sorted, min, max, str`), des méthodes  (`#!py .append`, ...).ou des modules avec le paramètre `SANS`. 

!!! example "Exemple 1" 
    Pour interdire l'usage de `#!py sum` dans l'exemple précédent, il suffit d'écrire :

    ```markdown     
    {% raw %}
    {{ IDE('scripts/somme2', SANS='sum') }}
    {% endraw %}
    ```

!!! example "Exemple 2" 
    Pour interdire l'usage de `#!py sum, max` et `#!py min`, il suffit d'écrire:

    ```markdown  
    {% raw %}
    {{ IDE('scripts/somme2', SANS='sum, max, min') }}
    {% endraw %}
    ```

??? abstract "Lien vers la documentation"
    [**Les paramètres de la macro IDE**](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/IDE-details/#la-macro-ide){ target="_blank"}


### Un IDE avec code, tests, correction et remarques
    
Pour rajouter des remarques qui s'afficheront avec la correction, on peut créer un fichier `.md` dont le nom est la concaténation du nom du fichier `.py` utilisé dans l'IDE (sans l'extension) et de `_REM.md` 

Lorsque les tests secrets sont validés ou lorque le compteur d'essais atteint 0, une admonition apparaît sous l'IDE avec le code du corrigé et les remarques. 

L'exemple suivant reprend le précédent, avec quelques paramètres optionnels et un fichier de remarques. Le contenu du ficher `somme3.py` est le même que celui de  `somme2.py` de l'exemple précédent.

!!! quote ""
    === "le code dans le fichier md"
    
        ```markdown
        Compléter le script ci-dessous.    
        N'oubliez pas de valider après avoir exécuté.    
        {% raw %}
        {{ IDE('scripts/somme3', MAX=3, SANS='sum') }}
        {% endraw %}
        ```

    === "contenu du fichier somme3_REM.md"
        ````markdown 
        On peut aussi parcourir le tableau `nombres` avec l'indice, mais c'est moins simple !
        ```python
        def somme(nombres):
            resultat = 0
            for indice in range(len(nombres)):
                resultat = resultat + nombres[indice]
            return resultat
        ```
        ````    

    === "Le résultat"

        Compléter le script ci-dessous.    
        N'oubliez pas de valider après avoir exécuté.    
                
        {{IDE('scripts/somme3', MAX=3, SANS='sum')}}    
    


!!! info "Remarque"
    Comme pour un terminal, il est possible de mettre à disposition des fonctions ou des classes.      
    Pour cela, il suffit d'ajouter (au début) dans votre fichier `nom_exo.py`, la section `env` :
    ```python
    # --------- PYODIDE:env --------- #
    
    ```


??? abstract "Lien vers la documentation"
    [**Tout sur les IDE**](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/IDE-details/){ target="_blank"}

## Exercices avec dessin

La syntaxe est spécifique pour ces deux types d'IDE.

### Tracé avec matplotlib (par Nicolas Revéret)

- Dans le fichier python, il faut adapter la section `env` en précisant l'`id` d'une `div` destinée à accueillir le tracé. (ici "cible_1"). Il faut un `id` différent par fenêtre de tracé.
- Dans le fichier `.md`, il faut créer, en html, sous l'IDE, une admonition contentant une `div` portant l'`id` de la zone graphique.

!!! quote ""
    
    === "le code du fichier md"
        ```markdown
        {% raw %}
        {{ IDE('scripts/exo_figure_1') }}
        {% endraw %}
        !!! quote ""
            <div id="cible_1" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le tracé sera affiché ici
            </div>  
        ```      
    === "le résultat"    
    
        {{ IDE('scripts/exo_figure_1')}}
    
        !!! quote ""
            <div id="cible_1" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le tracé sera affiché ici
            </div>   
    === "le code du fichier python"
        ```python
        # --- PYODIDE:env --- #
        # Import de matplotlib (installation lors du 1er lancement)
        import matplotlib

        # Précision du backend à utiliser
        matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

        # Insertion de la courbe dans une div spécifiée (id="cible_1")
        from js import document 
        document.pyodideMplTarget = document.getElementById("cible_1")
        # On vide la div
        document.getElementById("cible_1").textContent = ""

        # --- PYODIDE:code --- #

        import matplotlib.pyplot as plt

        fig, ax = plt.subplots() # Syntaxe obligatoire pour ne pas mélanger plusieurs graphiques
        xs = [-3 + k * 0.1 for k in range(61)]
        ys = [x**2 for x in xs]
        ax.plot(xs, ys, "r-")  # Syntaxe obligatoire pour ne pas mélanger plusieurs graphiques
        plt.grid()  # Optionnel : pour voir le quadrillage
        plt.axhline()  # Optionnel : pour voir l'axe des abscisses
        plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
        plt.title("La fonction carré")
        plt.show()
        ``` 
     



### Tracé avec turtle (par Romain Janvier)

- Dans le fichier python, il faut adapter la section `post` en précisant l'`id` d'une `div` destinée à accueillir le tracé. (ici "cible_2"). Il faut un `id` différent par fenêtre de tracé.
- Dans le fichier `.md`, il faut créer, en html, sous l'IDE, une admonition contentant une `div` portant l'`id` de la zone graphique.

!!! quote ""
    
    === "le code du fichier md"
        ```markdown
        {% raw %}
        {{ IDE('scripts/arbre_tortue') }}
        {% endraw %}
        !!! quote ""
            <div id="cible_2" class="admonition center" style="display: flex; justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le tracé sera affiché ici
            </div>  
        ```  
    === "le résultat"
        {{ IDE('scripts/arbre_tortue') }}
    
        <div id="cible_2" class="admonition center" style="display: flex; justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
        Le tracé sera affiché ici
        </div>
    
    === "le fichier python"
        ```python
        # --- PYODIDE:env --- #
        from js import document
        if "restart" in globals():
            restart()

        # --- PYODIDE:code --- #
        from turtle import *
        speed(10)

        def arbre(l=100, n=5):
            forward(l)
            if n > 0:
                left(45)
                arbre(l/2, n-1)
                right(90)
                arbre(l/2, n-1)
                left(45)
            back(l)

        arbre(200, 5)

        # --- PYODIDE:post --- #
        done()
        document.getElementById("cible_2").innerHTML = svg()
        ```
    
## Personnaliser les IDE et QCMs

Il est possible de personnaliser certains éléments de l'interface, comme par exemple, le noms de certaines admonitions, les info-bulles, ...

Pour cela :

* créer un fichier `main.py` à la racine du projet (dans le répertoire contenant le fichier `mkdocs.yml`) ;
* recopier le [code](documents/main.md){target="_blank"} du fichier original du fichier `main.py` de `pyodide_mkdocs_theme` ;  
* modifier les éléments que vous souhaitez. Voir la [documentation](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/messages/#terminaux-validation){target="_blank"}.

??? abstract "Le fichier `main.py` de ce site"
    Voici le contenu du fichier `main.py` de ce site dans lequel on a modifié certains messages dans les IDE et une info-bulle des QCMs.

    ```python
	from pyodide_mkdocs_theme.pyodide_macros import (
		PyodideMacrosPlugin,
		Msg, MsgPlural, TestsToken, Tip,
	)


	def define_env(env:PyodideMacrosPlugin):
		""" The customization has to be done at macro definition time.
			You could paste the code inside this function into your own main.py (or the
			equivalent package if you use a package instead of a single file). If you don't
			use personal macros so far, copy the full code into a `main.py` file at the root
			of your project (note: NOT in the docs_dir!).

			NOTE: you can also completely remove this file if you don't want to use personal
				  macros or customize the messages in the built documentation.

			* Change whatever string you want.
			* Remove the entries you don't want to modify
			* Do not change the keyboard shortcuts for the Tip objects: the values are for
			  informational purpose only.
			* See the documentation for more details about which string is used for what
			  purpose, and any constraints on the arguments:
			  https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/messages/

			---
            
			The signatures for the various objects below are the following:
			```python
			Msg(msg:str)
			MsgPlural(msg:str, plural:str="")
			Tip(width_in_em:int, msg:str, kbd:str=None)
			TestsToken(token_str:str)
            ```
			
		"""

		custom = { # contient uniquement ce qui a été modifié
		# Terminals
			"run_script":    Msg("Script lancé."),
			"install_start": Msg("Installation de paquets python en cours."),
			"install_done":  Msg("Installations terminées."),
			"success_msg":   Msg("Terminé sans erreur."),


		# Terminals: validation success/failure messages
			"success_head":  Msg("Bravo !"),
			"success_tail":  Msg("Vous pouvez lire"),
			"success_head_extra":  Msg("Tous les tests sont passés."),
			"fail_tail":     MsgPlural("est maintenant disponible", "sont maintenant disponibles"),


		# Corr  rems admonition:
			"corr":       Msg('Proposition de correction'),




		# QCMS
			"qcm_mask_tip":  Tip(15, "Les réponses resteront cachées.")
		}

		env.lang.overload(custom)
    ```