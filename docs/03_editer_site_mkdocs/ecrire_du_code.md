# Écrire du code 

!!! info "Les backticks"
 
	Pour mettre du texte en `mode code`, on utilise des **backticks** (ou **accents graves**) avant et après le texte.
	
	??? info "Où les trouver ?"
		On y accède par les touches <kbd>Alt Gr</kbd> + <kbd>è</kbd> du clavier Windows, et en accès direct sur un clavier mac.

## Code en ligne

### Sans coloration syntaxique

On peut placer tout contenu en `mode code`, sans faire référence à un langage de programmation. 
!!! example "Sans coloration syntaxique"
	=== "Le code"
		```markdown
		La variable `nombres` est de type `list` et la valeur référencée est `[1, 2, 3]` 
		```
	=== "Le résultat"
		La variable `nombres` est de type `list`  et la valeur référencée est `[1, 2, 3]`

### Avec coloration syntaxique

Si l'on précise un langage de programmation, la coloration syntaxique s'applique.    
La syntaxe est ``#!markdown `#!short_name_langage` `` où `short_name_langage` est [le nom court associé au langage](https://pygments.org/languages/).

!!! example "Avec coloration syntaxique"
	=== "Le code avec python"
		```markdown

		La variable `nombres` est de type `#!py list` et la valeur référencée est `#!py [1, 2, 3]` 
		```
  
	=== "Le résultat"
		La variable `nombres` est de type `#!py list` et la valeur référencée est `#!py [1, 2, 3]` 
	=== "Le code avec html"
		```markdown 

		Le contenu est entre les balises `#!html <body>` et `#!html </body>`  
		```
	=== "Le résultat" 
		Le contenu est entre les balises `#!html <body>` et `#!html </body>`    
		
!!! note "Remarques"
 	* Il faut aussi penser à rajouter un backtick (ouvrant et fermant) si le code à mettre en évidence en contient lui-même.  
	 Ici pour écrire ``#!markdown `#!short_name_langage ...` ``, on a dû 
	écrire :    
	```#!markdown ``#!markdown `#!short_name_langage` `` ```

## Bloc de code 
On crée un bloc de code en utilisant trois backticks ouvrants et trois backticks fermants. On peut faire suivre les backticks ouvrants du nom du langage pour activer la coloration syntaxique. 

### Sans coloration syntaxique
!!! example "Sans coloration"
	=== "Le code"
		````markdown

		```
		compteur prend la valeur 0
		pour nombre allant de 1 à 10
			ajouter nombre à compteur
		```
		````
	=== "Le résultat" 
		```
		compteur prend la valeur 0
		pour nombre allant de 1 à 10
			ajouter nombre à compteur
		```

### Avec coloration syntaxique

!!! example "Dans une console Python"
	=== "Le code"
		````markdown  

		```pycon
		>>> nombres = [3, 8, 7]
		>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
		```
		````
	=== "Le résultat" 
		```pycon
		>>> nombres = [3, 8, 7]
		>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
		```
 

!!! example "Dans un éditeur Python"
	=== "Le code"
		````markdown 
		```python
		print("Hello World")
		```
		````
	=== "Le résultat" 
		```python
		print("Hello World")
		```
 


### Bloc de code avec titre

!!! example "Dans un éditeur Python"
	=== "Le code"
		````md
		```python title="Bloc de code avec titre"
		print("Hello World!")
		```
		````

	=== "Aperçu"
		```python title="Bloc de code avec titre"
		print("Hello World!")
		```      

### Numérotation des lignes
On peut [afficher des numéros de ligne](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#adding-line-numbers){target='_blank'}, en ajoutant `linenums='1'` : 
!!! example "Dans un éditeur Python avec numéros de lignes"
	=== "Le code"
		````markdown  
		```python linenums='1'
		a = 1
		b = 2
		t = b
		b = a
		a = t
		```
		````
	=== "Le résultat" 
		```python linenums='1'
		a = 1
		b = 2
		t = b
		b = a
		a = t
		```

### Annotation du code

Les [annotations de code](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#adding-annotations){target=blank} sont positionnées dans un commentaire du langage concerné par le bloc de code. 
Elles apparaissent sous la forme d'un `+`, qui affiche un message au clic. 

!!! example "Annotations - première présentation"

    !!! danger "Attention"

        Bien veiller à mettre une ligne vide après la balise fermante `#!markdown  ``` ` et avant les annotations numérotées.

	=== "Le code"
    	````markdown 
		```python 
		note = 15
		if note >= 16:  # (1)
			print("TB")
		elif note >= 14:  # (2)
			print("B")
		elif note >= 12:  # (3)
			print("AB")
		elif note >= 10:
			print("reçu")
		else:
			print("refusé")
		```

		1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

		2. :warning: `elif` signifie **sinon si**.

		3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

		!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"
		
		````
		
	=== "Le résultat"
		```python 
		note = 15
		if note >= 16:  # (1)
			print("TB")
		elif note >= 14:  # (2)
			print("B")
		elif note >= 12:  # (3)
			print("AB")
		elif note >= 10:
			print("reçu")
		else:
			print("refusé")
		```

		1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

		2. :warning: `elif` signifie **sinon si**.

		3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

		!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"


!!! example "Annotations - deuxième présentation"
	=== "Le code"
		````markdown 
		!!! warning inline end "Important"

			Prenez le temps de lire les commentaires !

			Cliquez sur les +


		```python
			note = 15
			if note >= 16:  # (1)
				print("TB")
			elif note >= 14:  # (2)
				print("B")
			elif note >= 12:  # (3)
				print("AB")
			elif note >= 10:
				print("reçu")
			else:
				print("refusé")
		```

		1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

		2. :warning: `elif` signifie **sinon si**.

		3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.
		````
	
	=== "Le résultat"	
	
		!!! warning inline end "Important"

			Prenez le temps de lire les commentaires !

			Cliquez sur les +


		```python
			note = 15
			if note >= 16:  # (1)
				print("TB")
			elif note >= 14:  # (2)
				print("B")
			elif note >= 12:  # (3)
				print("AB")
			elif note >= 10:
				print("reçu")
			else:
				print("refusé")
		```

		1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

		2. :warning: `elif` signifie **sinon si**.

		3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.
	 
### Lignes de code surlignées

On peut également [surligner des lignes](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#highlighting-specific-lines){target='_blank'}, consécutives ou non, en couleur.
!!! example  "Surlignage de lignes de code"

     
    
	=== "Le code"
		````markdown  
		```python hl_lines="3 5-6"
		def maximum_tab(tableau):
			assert tableau != [], "le tableau est vide"
			maximum = tableau[0]
			for valeur in tableau:
				if valeur > maximum:
					maximum = valeur
			return maximum
		```
		````

	=== "Le résultat"	
		```python hl_lines="3 5-6" 
		def maximum_tab(tableau):
			assert tableau != [], "le tableau est vide"
			maximum = tableau[0]
			for valeur in tableau:
				if valeur > maximum:
					maximum = valeur
			return maximum
		```
