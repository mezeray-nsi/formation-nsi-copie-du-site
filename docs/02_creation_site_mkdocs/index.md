# Créer un site avec MkDocs

## Introduction

### Outils pour créer un site web statique

Plusieurs outils permettent de créer un site web statique. Parmi eux, [MkDocs](https://www.mkdocs.org/) est un outil simple et efficace pour créer un site web à partir de fichiers Markdown. Il peut être personnalisé avec des thèmes et des plugins, et est facile à déployer. [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) est un thème très complet pour MkDocs, qui offre une navigation agréable et des fonctionnalités avancées.

### Travail simplifié

Des collègues de l'association [AEIF](https://aeif.fr) nous ont facilité le travail en créant des sites "modèles", avec beaucoup de fonctionnalités utiles, des exemples et une documentation complète. Nous allons utiliser un de ces sites modèles pour créer notre propre site, en le clonant, en le personnalisant et en ajoutant notre propre contenu.

Le modèle choisi s'appuie sur [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/), en ajoutant les fonctionnalités de [Pyodide](https://pyodide.org/en/stable/) grâce au thème [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/). Le site permet ainsi de disposer d'éditeurs et de terminaux python et donc de proposer des exercices python en ligne, autocorrectifs.

Le modèle s'intitule "Site web de cours avec exercices Python dans le navigateur version pyodide-mkdocs-theme" et se situe sur la forge à l'adresse : [https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review)

## Mise en route

### Cloner le site modèle

Sur la page du dépôt, cliquer sur le bouton "Créer une bifurcation" pour créer un nouveau dépôt à partir du modèle. Le bouton n'est visible que si l'on est connecté sur la forge.

![Créer une bifurcation](screenshots/fork-01.png){.screen}

Dans le formulaire qui s'affiche, choisir :

-   le titre du site (qui sera aussi le nom du dépôt)
-   l'espace de nommage (votre nom, ou bien le nom d'un groupe [^1] que vous avez créé, ou auquel vous appartenez). Ce champ détermine l'URL du site, qui sera de la forme `https://[GROUPE|NOM].forge.apps.education.fr/[TITRE_SITE]`.
-   la branche à inclure (juste `main` suffit)
-   la visibilité du dépôt (**public**, privé ou interne) [^4]

[^1]: Les groupes sont des espaces de travail partagés, qui permettent de regrouper des projets et des membres. Pour créer un groupe, cliquer sur le bouton "+" dans le bandeau de gauche.
[^4]: Les **dépôts** privés ne sont accessibles qu'aux membres du groupe ou aux personnes invitées. Les dépôts internes ne sont accessibles qu'aux personnes identifiées sur la Forge. Dans ces deux cas, si vous souhaitez que le site soit public, il faudra le rendre accessible à tout le monde après le déploiement. Voir [Configuration de l'URL](#configuration-de-lurl).

![Créer une bifurcation - paramétrage](screenshots/fork-02.png){.screen}

Au bout d'un certain temps, le dépôt est créé.

![Résultat du clonage](screenshots/fork-03.png){ .screen}

### Déployer le site

En tant que tel, le site n'est pas encore accessible en ligne. La phase de déploiement permet de générer les fichiers HTML à partir des fichiers Markdown, et de les rendre accessibles sur le web. Cette étape se fait lors de l'exécution d'un script (le fichier `.gitlab-ci.yml`, fourni dans le modèle) sur les serveurs de la Forge (étapes de CI[^2]/CD[^3]).

[^2]: Intégration continue
[^3]: Déploiement continu

#### Déclenchement du CI

Pour déclencher le CI, il faut naviguer vers "Compilation" > "Pipelines" dans le dépôt.

![Bouton pipeline](screenshots/pipeline-01.png){width=70% .center .screen}

Cliquer sur "Exécuter le pipeline" pour lancer la compilation et la première publication du site.

![Exécution du pipeline](screenshots/pipeline-02.png){width=70% .center .screen}

Puis cliquer sur "Exécuter" pour confirmer.

![Confirmation de l'exécution](screenshots/pipeline-03.png){width=70% .center .screen}

Au bout d'un certain temps, le pipeline est exécuté et les drapeaux passent au vert.

![Pipeline en cours](screenshots/pipeline-04.png){width=49% .screen}
![Pipeline terminé](screenshots/pipeline-05.png){width=49% .screen}

#### Configuration de l'URL

Naviguer vers "Déploiement > Pages" pour récupérer l'URL du site et la configurer.
![Déploiement des pages](screenshots/pages-01.png){width=70% .center .screen}

L'URL du site est affichée dans la page.

![URL du site](screenshots/pages-02.png){.screen}

L'URL par défaut est assez compliquée, pour plus de lisisbilité, il est possible d'utiliser une adresse plus simple. Pour cela, décochez la case "Utiliser un domaine unique" et enregistrez les modifications.

![Configuration de l'URL](screenshots/pages-03.png){.screen}

Le site est maintenant accessible à l'adresse `https://[GROUPE|NOM].forge.apps.education.fr/[TITRE_SITE]`.

Dans le cas particulier où le dépôt est **privé ou interne**, et où l'on souhaite que le site soit public, il faut modifier la visibilité du dépôt pour qu'il soit accessible à tout le monde. Pour cela, naviguer vers "Paramètres > Général > Visibilité, fonctionnalités du projet, autorisation", descendre vers la section "Pages" et choisir "Tout le monde" dans le menu déroulant.

![Visibilité du dépôt](screenshots/pages-04.png){.screen}

## Personnaliser le site

### Passer en mode édition

Pour éditer le site, deux solutions existent : soit en local, soit directement sur la Forge ; nous commencerons par la deuxième solution.

Naviguer vers l'accueil du dépôt, (via par exemple la ligne de vie en haut du site), puis cliquer sur le bouton "Modifier > Web IDE" pour passer en mode édition.

![Passer en mode édition](screenshots/edition-01.png){width=70% .center .screen}

![Bouton mode édition](screenshots/edition-02.png){width=70% .center .screen}

![Editeur](screenshots/edition-03.png){.screen}

L'éditeur (une version web de Microsoft Visual Studio Code) s'ouvre, avec les fichiers du dépôt. On peut alors modifier les fichiers Markdown, les images, les fichiers de configuration, etc.

### Arborescence des fichiers

![Arborescence des fichiers](screenshots/edition-04.png){width=50% .center .screen}

-   `docs/` : contient les fichiers Markdown qui seront convertis en pages HTML, c'est dans ce dossier que nous allons ajouter nos propres pages. Les pages existantes sont des exemples, on peut les utiliser comme inspiration pour créer nos propres pages.
-   `mkdocs.yml` : fichier de configuration de MkDocs, nous devrons le modifier pour personnaliser le site.
-   `requirements.txt` : liste des dépendances Python nécessaires pour le déploiement, nous n'aurons pas besoin de le modifier.
-   `.gitlab-ci.yml` : fichier de configuration de GitLab CI/CD, contient les scripts de construction et de déploiement. Nous n'aurons pas besoin de le modifier.
-   `.gitlab` : dossier contenant des fichiers de configuration pour GitLab, nous n'aurons pas besoin de les modifier, nous pouvons même le supprimer.
-   `.README.md` : fichier qui précise le contenu affiché sur la Forge, lorsqu'on consulte le dépôt du projet. Il est recommandé de le modifier.

Dans le dossier `docs/`, on trouve un fichier important : le fichier `.pages`. Ce fichier gère la navigation du site, il contient la liste des pages à afficher dans le menu de navigation. Il est possible de le modifier pour ajouter, supprimer ou déplacer des pages.

### Fin de la configuration du site

-   Renommer (clic droit sur le dossier) le dossier `docs/` en `docs_exemples/`.
-   Créer un nouveau dossier (clic droit dans le panneau latéral gauche) `docs/` pour y mettre ses propres fichiers Markdown.
-   Ouvrir le fichier `mkdocs.yml` et modifier :
    -   `site_name` : le nom du site
    -   `site_description` : la description du site
    -   `copyright` : le copyright (nom de l'auteur et licence)
-   Ouvrir le fichier `README.md` et modifier le contenu, rédigé en markdown, par exemple en créant un lien vers le site, ce qui permettra de naviguer facilement de la Forge vers le site.

!!! abstract "Exemple de fichier `README.md`"

    ```md
    # Présentation de [TITRE_SITE]

    Ce site présente ... à destination de ...
    [Lien vers le site](URL_SITE)
    ```


### Ajouter du contenu

Créer un fichier `index.md` dans le dossier `docs/` pour la page d'accueil, et y ajouter du contenu rédigé en markdown.

!!! abstract "Exemple de fichier `index.md`"

    ```md
    # Bienvenue sur notre site

    Ceci est la page d'accueil de notre site.
    Vous pouvez y ajouter du contenu, des images, des liens, etc.

    ```

Créer aussi le fichier `.pages` dans le dossier `docs/` pour définir la navigation du site. La syntaxe de ce fichier est la suivante :

-   `title`: le titre du menu dans la barre de navigation
-   `nav`: la liste des pages (fichiers markdown ou sous-dossier) à afficher dans le menu de navigation

!!! abstract "Exemple de fichier `.pages`"

    ```yaml

    title: Titre site
    nav: - index.md

    ```

### Sauvegarde et publication

Pour sauvegarder les modifications, il suffit de cliquer sur le bouton "Source Control" ![Bouton](screenshots/edition-06.png){width=50px} en bas à gauche de l'éditeur.

![Bouton commit](screenshots/edition-05.png){width=70% .center .screen}

Ajouter un message de **commit** pour décrire les changements effectués.

![Message de commit](screenshots/edition-07.png){width=70% .center .screen}

Puis cliquer sur le bouton "Commit to main" pour enregistrer les modifications et lancer une construction du site. Confirmer le commit dans la boîte de dialogue qui s'affiche.

![Confirmation du commit](screenshots/edition-08.png){width=70% .center .screen}

Le site est alors reconstruit ; la construction est visible dans la section "Compilation > Pipelines" du dépôt.
