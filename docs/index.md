---
title: Accueil
hide:
    -toc
---

# forge.apps.education.fr

![](./assets/LogoLaForge.svg){ width=50% }![](./assets/qrcode_atelier_forge.png){ width=20% align=right}

Ce site est le document d'accompagnement de la formation "Forge des Communs Numériques Éducatifs" proposée par le PCD NSI Normandie (juillet 2024).

## Sommaire

-   [Présentation de la forge](./01_presentation_forge/index.md)
-   [Créer un site en utilisant un modèle](./02_creation_site_mkdocs/index.md)
-   [Éditer son site](./03_editer_site_mkdocs/markdown.md)
-   [Atelier formation](./04_atelier_formation/etape_1.md)
-   [Git en pratique](./05_git_pratique/index.md)
