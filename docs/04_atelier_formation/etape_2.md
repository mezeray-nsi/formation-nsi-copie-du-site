---
title: Étape 2
---

# Créer un exercice avec l'IDE python

!!! question "Question 1"
Créer sur son site une page comprenant un sujet d'exercice, avec un IDE python, des tests publics, des tests privés, un corrigé.  
 On utilisera _a minima_ les sections `code`, `corr`, `tests`, `secrets` dans le fichier python de l'exercice, et de façon optionnelle une section `env` et/ou un fichier de remarques.  
 Voir la page [des exercices avec IDE](../03_editer_site_mkdocs/exercices.md) pour les détails de syntaxe.
??? note "Suggestions"

        Si vous manquez d'inspiration, vous pouvez analyser et reproduire ou adapter l'un des exercices suivants, issus du site Codex :

        - un exercice de [recherche de couples dans une liste](https://codex.forge.apps.education.fr/exercices/nb_puis_double/) ;
        - un exercice sur [valeurs et indices](https://codex.forge.apps.education.fr/exercices/indices_valeurs/), avec des fonctions à compléter et un fichier de remarques ;
        - un exercice de [recherche de maximum](https://codex.forge.apps.education.fr/exercices/deux_meilleurs/) avec deux versions proposées dont l'une à trous, avec des fonctions interdites et une solution alternative en remarque ;
        - un exercice de [déchiffrement 'zen'](https://codex.forge.apps.education.fr/exercices/mra_bs_clguba/), avec une section `env` pour charger la valeur de variables, et un fichier de remarques ;
        - un exercice d'[évaluation postfixe](https://codex.forge.apps.education.fr/exercices/eval_postfixe/), avec une section `env` pour charger la classe, deux versions proposées dont l'une à trous, et des fichiers de remarques adaptés à chaque version ;
        - ou tout exercice de votre choix.

!!! question "Question 2"
Ajouter à votre feuille d'exercices un QCM, portant sur l'exercice précédent.  
 Voir la page [des QCMs](../03_editer_site_mkdocs/faire_QCM.md) pour les détails de syntaxe.

!!! question "Question 3"
Échanger l'url du site avec des collègues et tester les exercices des collègues.
