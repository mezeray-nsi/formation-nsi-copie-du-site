---
title: La Forge
---

# Présentation de la Forge des Communs Numériques Éducatifs

## Qu'est-ce "la forge des communs numériques éducatifs" ?

<figure markdown="span">
  ![Brigit & Komit](images/Brigit_Komit.png){ width=40% }
  <figcaption>Illustration CC BY <a href="https://juliettetaka.com/fr">Juliette Taka</a>
  </figcaption>
 ![Logo La Forge](images/LogoLaForge.svg){ width=40% }
</figure>

 Proposée par le ministère de l'éducation nationale, la [forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/) contient des ressources pédagogiques (logiciels et ressources éducatives libres) utiles à la communauté scolaire. Elle repose sur le logiciel libre GitLab.

!!! info
    Une forge informatique est un outil en ligne permettant de travailler à plusieurs sur des projets de logiciel, mais elle peut également servir à l’édition collaborative et à la publication de ressources numériques comme des sites web.

## Quelques exemples de ressources en lien avec NSI  

### Sites web de cours ou exercices pour NSI et/ou SNT

- [CodEx](https://codex.forge.apps.education.fr/){target='_blank'} : le code par les exercices, par une communauté de professeurs qui enseignent SNT et NSI ([dépôt](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr){target='_blank'})
- [Dépôt](https://forge.apps.education.fr/nreveret){target='_blank'} de Nicolas Reveret comportant plusieurs dépôts de sites (bdd, tris, dictionnaires, etc.)

#### SNT

- [Enseigner les SNT](https://ressources.forge.apps.education.fr/snt/){target='_blank'} par l'association AEIF ([dépôt](https://forge.apps.education.fr/ressources/snt){target='_blank'})

#### Première NSI

- [Première NSI](https://cours-nsi.forge.apps.education.fr/premiere/){target='_blank'} par Pascal Remy et Charles Poulmaire ([dépôt](https://forge.apps.education.fr/cours-nsi/premiere){target='_blank'})

#### Terminale NSI
- [Terminale NSI](https://cours-nsi.forge.apps.education.fr/terminale/){target='_blank'} par Mathilde Boehm, Pascal Remy et Charles Poulmaire ([dépôt](https://forge.apps.education.fr/cours-nsi/terminale){target='_blank'})
- [Terminale NSI Lycée du Parc](https://fjunier.forge.apps.education.fr/tnsi/){target='_blank'} par Frédéric Junier ([dépôt](https://forge.apps.education.fr/fjunier/tnsi){target='_blank'})
- [Terminale NSI au lycée Saint-Aspais](https://mcoilhac.forge.apps.education.fr/term/){target='_blank'} par Mireille Coilhac ([dépôt](https://forge.apps.education.fr/mcoilhac/term){target='_blank'})
- [Terminale NSI](https://cours.forge.apps.education.fr/terminale-nsi/){target='_blank'} par Angélique Beucher ([dépôt](https://forge.apps.education.fr/cours/terminale-nsi){target='_blank'})

### Logiciels et outils

- [La Nuit du Code](https://www.nuitducode.net/){target='_blank'}, marathon de programmation durant lequel les élèves ont 6h pour coder un jeu avec Scratch ou Python, par équipes de deux ou trois ([dépôt](https://forge.apps.education.fr/nuit-du-code){target='_blank'})
- [My mark map](https://mymarkmap.forge.apps.education.fr/){target='_blank'} : outil pour créer des cartes mentales en ligne, par Cédric Eyssette ([dépôt](https://forge.apps.education.fr/myMarkmap/myMarkmap.forge.apps.education.fr))
- [MathALÉA](https://coopmaths.fr/alea/){target='_blank'}  : outil pour créer des listes d'exercices à données aléatoires, sous différents formats, par CoopMaths ([dépôt](https://forge.apps.education.fr/coopmaths/mathalea/){target='_blank'})

### Documentation

- Documentation de Capytale : [site web](https://capytale.forge.apps.education.fr/basthon/basthon-capytale/index.html){target='_blank'} (en construction)

D'autres propositions sont disponibles sur la page d'accueil de la [forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/#ressources-de-laforgeedu).
