# Principes de git

## Caractéristiques

### Décentralisé

- chaque développeur a une copie complète de l'historique
- chaque développeur peut travailler en local
- chaque développeur peut publier ses modifications

### Fusion

- chaque développeur peut travailler en parallèle
- git gère les conflits lors de la fusion du code de plusieurs développeurs

## Fonctionnement

### Stockage des données

Stockage des données = objets :

- blobs (fichiers, nommés par le sha de leur contenu, ou bien empaquetés dans des "packs")
- trees (arborescence)
- commits (instantanés = references vers des trees)
- tags (étiquettes)

### Organisation des données

Stocke les données sous forme de "snapshots" (instantanés) de l'ensemble du code via des références / liens entre les objets.

TODO: schema

###

3 états :

- working directory : le code que vous modifiez
- staging area : les modifications que vous avez préparées pour le prochain commit
- repository : l'historique de tous les commits

TODO: diagrammes
