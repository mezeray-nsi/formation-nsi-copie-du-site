# Premier test

Nous allons tester les principales étapes de l'utilisation de Git sur un dépôt distant. Pour cela, vous pouvez utiliser un dépôt sur lequel vous avez les droits d'écriture, que ce soit le dépôt de votre projet de site web, ou un [nouveau dépôt de test](./clone-private/index.md)

## Premier clonage

Récupérer sur la page du projet le lien de clonage par SSH : bouton "Code", puis "Cloner avec SSH". Copier l'URL.

<figure>
<img src="../screenshots/usages/clone/ssh-clone-01.png"  class="img-fluid w-full on-glb screen">
<figcaption class="text-sm">Copier l'URL de clonage par SSH</figcaption>
</figure>

Dans un terminal, naviguer vers le dossier où vous voulez cloner le dépôt, et lancer la commande `git clone <URL> [<dest>]`. Si vous ne précisez pas de destination, le dépôt sera cloné dans un dossier portant le nom du dépôt.

Lors du tout premier clonage, le serveur ssh envoie sa clef publique au client pour qu'il puisse vérifier l'identité du serveur. Si le serveur est inconnu, le client demande à l'utilisateur s'il accepte d'ajouter l'hôte à la liste des connus. Si l'utilisateur accepte, le serveur est ajouté à la liste des connus, et le clonage peut continuer. Confirmez en tapant `yes`, ou `y` et validez.[^3]

[^3]: Cette étape n'est à faire qu'une seule fois, pour chaque serveur. Elle permet de vérifier que le serveur auquel on se connecte est bien celui qu'on croit, et non un serveur malveillant (attaque de type Man In The Middle).

<figure>
<img src="../screenshots/usages/clone/ssh-clone-02.png"  class="img-fluid w-full on-glb screen ">
<figcaption class="text-sm"><code>git clone &lt;URL> [&lt;dest>]</code></figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/ssh-clone-03.png"  class="img-fluid w-full on-glb screen">
<figcaption class="text-sm">confirmer l'ajout de l'hôte à la liste des connus</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/ssh-clone-04.png"  class="img-fluid w-full on-glb screen">
<figcaption class="text-sm">Clonage réussi !</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/ssh-clone-05.png"  class="img-fluid w-full on-glb screen">
<figcaption class="text-sm">Le dossier est bien présent</figcaption>
</figure>

## Tester un cycle de modifications

Maintenant que vous avez cloné le dépôt, vous pouvez tester un cycle de modifications :

-   modifiez un fichier (par exemple le fichier `README.md`) dans le dépôt cloné. Utilisez l'éditeur de texte de votre choix.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-01.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Naviguer vers le projet sur l'ordi, et éditer le fichier README.md</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-02.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Ouvrir avec un éditeur de texte (notepad suffit)    </figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-03.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Rajouter une ligne / modifier le fichier</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-04.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Enregistrer la modification</figcaption>
        </figure>

-   dans le terminal, la commande `git status` vous permet de voir l'état du dépôt : quels fichiers ont été modifiés, quels fichiers sont prêts à être enregistrés, quels fichiers ne sont pas suivis par Git.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-05.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Commande <code>git status</code> pour voir l'état du dépôt</figcaption>
        </figure>

-   la commande `git add <fichier>` permet de "préparer" l'enregistrement des modifications du fichier `<fichier>`. Vous pouvez ajouter plusieurs fichiers en une seule commande (`git add <fichier1> <fichier2>...`), ou ajouter tous les fichiers modifiés d'un répertoire et de ses sous-répertoires de manière récursive en une seule commande avec `git add <repertoire>`. La commande `git add .` ajoute tous les fichiers modifiés du répertoire courant et de ses sous-répertoires.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-06.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">On prépare l'enregistrement des modifications pour le fichier <code>README.md</code></figcaption>
        </figure>

-   `git commit -m "message"` permet d'"enregistrer les modifications" qui avaient été préparées, associées à votre identité et au message de commit `"message"`. Si l'étape `git config ...` pour configurer son identité n'a pas encore été faite, elle sera demandée à ce moment.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-07.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">On "enregistre" les modifications avec <code>git commit -m "message"</code>... MAIS erreur car 1ère fois</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-08.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Éventuellement, on répond à la demande de git de savoir qui on est (nom et email)</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-09.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Cette fois le <code>git commit -m "message"</code> fonctionne</figcaption>
        </figure>

-   Les modifications ont été enregistrées, on peut utiliser `git push` pour "envoyer" ces modifications sur le dépôt distant.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-10.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Et on peut envoyer ce lot de modifications sur le serveur</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-11.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">En rafraîchissant la page on verra les modifications</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-12.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Les modifications sont là : fichier, hash du commit, etc...</figcaption>
        </figure>

-   Pour récupérer des changements effectués par vous ou une autre personne, nous allons modifier un fichier directement sur GitLab pour créer un nouveau jeu de modifications (mais cela peut très bien provenir d'une autre personne avec qui vous travaillez sur le même dépôt, ou bien de vous même sur un autre poste de travail...). Naviguez vers le fichier "README.md" et cliquez sur le bouton "Modifier". Ajoutez une ligne, et validez les modifications. Il vous sera demandé un message de commit. Validez. Des modifications sont maintenant présentes sur le dépôt distant, et pas chez vous.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-13.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Cliquer sur le fichier README.md, et bouton "Modifier"    </figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-14.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Modifier le fichier unique</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-15.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Rajouter une autre ligne, modifier le fichier</figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-16.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Editer le message de commit, et valider les modifications    </figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-17.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Nouveau commit, avec modifications</figcaption>
        </figure>

-   Sur votre machine, vous pouvez utiliser `git pull` pour "récupérer" les modifications du dépôt distant.

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-18.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Récupérations de ces modifications avec <code>git pull</code></figcaption>
        </figure>

        <figure>
        <img src="../screenshots/usages/commit/commit-19.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm">Les modifications sont bien arrivées</figcaption>
        </figure>

-   Enfin, vous pouvez utiliser `git log` pour voir l'historique des modifications :

    ???info "Les étapes en images"

        <figure>
        <img src="../screenshots/usages/commit/commit-20.png"  class="img-fluid w-full on-glb screen">
        <figcaption class="text-sm"><code>git log</code> nous donne bien l'historique des modifications</figcaption>
        </figure>
