# Dépôt public, lecture seule

## Récupérer l'URL du dépôt

Naviguer vers le dépôt _public_ sur la forge, gitlab, github, et copier l'URL du dépôt en **https**, disponible sur la page d'accueil du dépôt, en cliquant sur le bouton "Code".

<figure>
<img src="../screenshots/usages/clone/clone-public-https-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Sur la page d'accueil du dépôt, bouton "Code"</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-public-https-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Copier l'URL en https</figcaption>
</figure>

## Cloner le dépôt

Ouvrir un terminal, et taper la commande `git clone` suivi de l'url du dépôt, et optionnellement du nom du dossier de destination. Si vous ne précisez pas de nom de dossier, git créera un dossier portant le nom du dépôt.

`git clone <URL> [<dest>]`

<figure>
<img src="../screenshots/usages/clone/clone-public-https-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Ligne de commande, éventuellement précédée de `cd ......`</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-public-https-04.png" class="img-fluid w-full">
<figcaption class="text-sm">Clonage réussi...</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-public-https-05.png" class="img-fluid w-full">
<figcaption class="text-sm">Dossier présent...</figcaption>
</figure>

## Récupérer les mises à jour

Pour récupérer les mises à jour du dépôt distant, il suffit de se placer dans le dossier du dépôt, et de taper la commande `git pull`.

Si des modifications ont été apportées au dépôt distant, elles seront récupérées et fusionnées dans votre copie locale.

Si vous avez apporté des modifications à votre copie locale, et que des modifications ont été apportées au dépôt distant, git tentera de fusionner les modifications. Si des conflits surviennent, git vous demandera de les résoudre avant de continuer (cf partie "résolution de conflits", plus tard).
