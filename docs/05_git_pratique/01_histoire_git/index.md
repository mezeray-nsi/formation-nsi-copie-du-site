# Logiciel de Gestion de Versions

## Utilité

Un logiciel de gestion de versions (VCS en anglais) est un logiciel qui répond aux problématiques de travail collaboratif sur un projet informatique. En effet, lorsqu'on travaille à plusieurs sur un projet, il est nécessaire de pouvoir :

-   **versionner son code** : qui a fait quoi, quand, pourquoi, comment. Permet de revenir à une version précédente, de comparer des versions pour savoir ce qui a changé, où un bug est apparu, etc..
-   **résoudre ou éviter les conflits** : travailler à plusieurs sur le même code, en même temps ou en alternance, peut générer des conflits. Il faut alors les résoudre, ou les éviter.

Lorsqu'on travaille seul, un VCS est tout aussi utile pour garder un historique de son code, et pour pouvoir revenir à une version antérieure en cas de problème.

Sans outil spécifique, on peut mettre en place des moyens "primitifs" pour gérer son code, comme un ftp / email / disquette / clef usb, et des procédures pour éviter ou résoudre les conflits. Mais ces moyens sont limités, et ne permettent pas de gérer efficacement un projet de taille moyenne ou grande.

On utilise alors un outils de gestion de version. Il en existe plusieurs, qui se distinguent par :

-   mode de stockage / accès : centralisé / décentralisé
-   mode de gestion des potentiels conflits : fusion / verrouillage

## Les ancètres

Les ancêtres, centralisés ou strictement locaux :

-   CSV (1990) : centralisé / fusion
-   SVN (2000) : centralisé / fusion et/ou verrouillage

Les années 2002-2005 voient une explosion de VCS décentralisés, qui permettent de travailler en mode déconnecté, et de fusionner les modifications :

-   Arch (2002) : décentralisé / fusion
-   Darcs (2003) : décentralisé / fusion
-   Bazaar (2005) : décentralisé / fusion
-   Mercurial (2005) : décentralisé / fusion
-   Git (2005) : décentralisé / fusion

## Histoire de Git

Racontée dans [git-scm.com](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Une-rapide-histoire-de-Git)

> Le noyau Linux est un projet libre de grande envergure. Pour la plus grande partie de sa vie (1991–2002), les modifications étaient transmises sous forme de patchs et d’archives de fichiers. En 2002, le projet du noyau Linux commença à utiliser un DVCS propriétaire appelé BitKeeper.

> En 2005, les relations entre la communauté développant le noyau Linux et la société en charge du développement de BitKeeper furent rompues, et le statut de gratuité de l’outil fut révoqué. Cela poussa la communauté du développement de Linux (et plus particulièrement Linus Torvalds, le créateur de Linux) à développer son propre outil en se basant sur les leçons apprises lors de l’utilisation de BitKeeper. Certains des objectifs du nouveau système étaient les suivants : vitesse, conception simple, support non-linéaire (des milliers de branches parallèles) et entièrement distribué, capacité à gérer efficacement des projets d’envergure tels que le noyau Linux (vitesse et compacité des données).

> Depuis sa naissance en 2005, Git a évolué et mûri pour être facile à utiliser tout en conservant ses qualités initiales. Il est incroyablement rapide, il est très efficace pour de grands projets et il a un incroyable système de branches pour des développements non linéaires

## Écosystème

En plus des fonctionnalités propres de Git, tout un écosystème de services et d'outils s'est développé autour de Git, permettant de faciliter le travail collaboratif et la gestion de projet.

-   2008 : bitbucket (rachat par Atlassian en 2010)
-   2008 : github (rachat par Microsoft en 2018)
-   2012 : gitlab

Git est aujourd'hui de-facto standard pour l'industrie du logiciel

-   github, gitlab, bitbucket, etc..
-   intégration continue, déploiement continu, etc..
-   gestion de tickets, wiki, etc..
