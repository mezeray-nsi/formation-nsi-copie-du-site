# Site dans linux


## Étapes Préliminaires

* Vérifier si `git` est installé
```
git --version
```

* Si `git` n'est pas installé, se mettre en root et installer `git` 
```
apt install git
```
* se mettre en utilisateur et vérifier que git est installé
```
git --version
```
* configurer git avec votre nom et votre email
```
git config --global user.name "John Doe"
git config --global user.email john.doe@ac-normandie.fr
```
* aller dans le répertoire personnel et vérifier la possession d'une clé SSH
``` 
cd
ls -al .ssh/
```
* si le répertoire n'existe pas vous n'en avez pas et il faut créer une paire de clefs SSH (ou en créer une nouvelle)
```
ssh-keygen
```
    * accepter le chemin par défaut (si vous n'aviez pas de clé) ou en mettre un autre en changeant le nom `id_...` en appuyant sur `Entrée`
    * appuyer  sur `Entrée` pour ne pas mettre de mot de passe (ou en mettre un si vous le souhaitez)

* afficher votre clef publique (remplacer `xxx` par le nom adéquat)
```
cat .ssh/id_xxx.pub
```
* Aller sur la forge, cliquer sur l’icône `utilisateur` puis dans `préférences` choisir `clés SSH`
* ajouter une clé et copier le contenu de `id_xxx.pub` et le coller dans le champs `Clé` puis valider.

!!! warning "Attention"
    Si vous travailler avec un autre ordinateur, il faudra ajouter la clé publique lié à celui-ci.


## Cloner un dépôt avec la clé SSH

* Dans un terminal, se déplacer dans le répertoire qui contiendra celui du dépôt ou indiquer `<dest>` dans la commande
* Copier avec SSH l'url du dépôt 
* cloner avec git
``` 
git clone <URL>  
```
ou 
```
git clone <URL> <dest>
```
* Vérifier dans un explorateur de fichiers que le dossier du dépôt a été crée avec tout ce qu'il faut.
*  modifier un fichier (le `README.md`) dans le dépôt cloné (dans l'éditeur de votre choix)
* `git status` pour voir les modifications
* `git add <fichier>` pour "préparer" l'enregistrement des modifications
* `git commit -m "message"` pour "enregistrer" les modifications
* `git config ...` pour configurer son identité si ce n'est pas déjà fait
* `git push` pour "envoyer" les modifications sur le dépôt distant
* modifier le fichier dans GitLab, enregistrer ces modifications
* `git pull` pour "récupérer" les modifications du dépôt distant
* `git log` pour voir l'historique des modifications

## Travailler en local

* Ouvrir un terminal et se placer dans le dossier de votre dépôt

* **Étape 0** : si vous avez modifié le dépôt en ligne ou si vous travaillez à plusieurs
```
git pull
```

* **Étape 1** :   
```
python -m venv venv
```
* **Étape 2** :   
```
source venv/bin/activate
```
 * **Étape 3** :   
```
pip install -r requirement.txt
```
* **Étape 4** :   
```
mkdocs serve
```
* **Étape 5** : ouvrir un navigateur et saisir l'url `http://localhost:8000` pour voir le site et les modifications au fur et à mesure.

!!! warning "Attention"
    
    * Faire les 4 étapes la première fois
    * Faire l'étape 2 et l'étape 4 les fois suivantes 
    * si le fichier `requirement.txt` est modifié, il faudra faire l'étape 2, l'étape 3 et l'étape 4 

## Travailler avec VS Code

### Installation
* Télécharger le fichier `code_xxx.deb` 
* Dans un terminal, se mettre en root et à la racine
``` 
cd su ~
```
* installer `code_xxx.deb`
``` 
dpkg -i dossier/code_xxx.deb
```

### Modification du dépôt en local
* Ouvrir VS Code
* Ouvrir le dossier du dépôt : vous avez le même environnement qu'en ligne
* Ouvrir un terminal dans VS Code (`Terminal` ...), il suffit de faire les étapes 2, 4 et 5 pour voir les modifications au fur et à mesure.

### Mise à jour du dépôt

* cliquer sur l'icône `Source Control` : vous y verrez les changements
* cliquer sur `Commit` après avoir préciser un message
* ajouter les changements puis cliquer sur `Commit` 
* cliquer sur `Sync Changes ...`  si vous êtes sur de vous !